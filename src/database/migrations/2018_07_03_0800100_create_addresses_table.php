<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() : void
    {
        try{
            Schema::create(config('sam-address.table'), function (Blueprint $table) {
                $table->increments('id');
                $table->morphs('addressable');
                $table->timestamps();
                $table->string('label');
                $table->string('firstname')->nullable();
                $table->string('lastname')->nullable();
                $table->string('company')->nullable();
                $table->string('street');
                $table->string('street_number');
                $table->string('place_number')->nullable();
                $table->string('post_code');
                $table->string('city');
                $table->string('country_code', 2)->nullable();
                $table->string('phone_number')->nullable();
                $table->string('email')->nullable();
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \Totem\SamAddress\Database\Seeds\AddressesSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() : void
    {
        Schema::dropIfExists(config('sam-address.table'));
    }
}

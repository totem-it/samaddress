<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */

$factory->define(\Totem\SamAddress\App\Model\Address::class, function (Faker $faker) {
    return [
        'addressable_type'  => $faker->randomElement(\Totem\SamAddress\App\Enums\AddressType::getValues()),
        'addressable_id'    => $faker->unique()->numberBetween(1, 50),
        'label'             => $faker->name,
        'firstname'         => $faker->firstName,
        'lastname'          => $faker->lastName,
        'company'           => $faker->optional()->company,
        'street'            => $faker->streetName,
        'street_number'     => $faker->buildingNumber,
        'place_number'      => $faker->optional()->randomDigit,
        'post_code'         => $faker->postcode,
        'city'              => $faker->city,
        'country_code'      => $faker->optional(0.3, 'pl')->languageCode,
        'phone_number'      => $faker->unique()->phoneNumber,
        'email'             => $faker->unique()->companyEmail,
    ];
});

$factory->state(\Totem\SamAddress\App\Model\Address::class, 'billing', function () {
    return [
        'addressable_type' => \Totem\SamAddress\App\Enums\AddressType::Billing
    ];
});

$factory->state(\Totem\SamAddress\App\Model\Address::class, 'shipping', function () {
    return [
        'addressable_type' => \Totem\SamAddress\App\Enums\AddressType::Shipping
    ];
});
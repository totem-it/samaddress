<?php

namespace Totem\SamAddress\Database\Seeds;

use Illuminate\Database\Seeder;

class AddressesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        if (config('app.env') !== 'production') {
            factory(\Totem\SamAddress\App\Model\Address::class, 50)->create();
        }
    }
}

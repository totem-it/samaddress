<?php

Route::group(['prefix' => 'api' ], function() {

    Route::middleware(config('sam-customers.guard-api'))->group( function() {

        Route::group(['prefix' => 'profile/address-book'], function() {
            Route::get('/', 'Totem\SamAddress\App\Controllers\ApiAddressController@index');
            Route::get('billing', 'Totem\SamAddress\App\Controllers\ApiAddressController@indexBilling');
            Route::get('shipping', 'Totem\SamAddress\App\Controllers\ApiAddressController@indexShipping');
            Route::get('{id}', 'Totem\SamAddress\App\Controllers\ApiAddressController@show')->where('id', '[0-9]+');
            Route::post('billing', 'Totem\SamAddress\App\Controllers\ApiAddressController@createBilling');
            Route::post('shipping', 'Totem\SamAddress\App\Controllers\ApiAddressController@createShipping');
            Route::put('{id}', 'Totem\SamAddress\App\Controllers\ApiAddressController@replace')->where('id', '[0-9]+');
            Route::delete('{id}', 'Totem\SamAddress\App\Controllers\ApiAddressController@destroy')->where('id', '[0-9]+');
        });

    });

    Route::middleware(config('sam-admin.guard-api'))->group( function() {

        Route::group(['prefix' => 'customers/addresses'], function() {
            Route::get('/', 'Totem\SamAddress\App\Controllers\ApiAddressController@index');
            Route::get('billing', 'Totem\SamAddress\App\Controllers\ApiAddressController@indexBilling');
            Route::get('shipping', 'Totem\SamAddress\App\Controllers\ApiAddressController@indexShipping');
            Route::get('{id}', 'Totem\SamAddress\App\Controllers\ApiAddressController@show')->where('id', '[0-9]+');
            Route::post('/', 'Totem\SamAddress\App\Controllers\ApiAddressController@create');
            Route::put('{id}', 'Totem\SamAddress\App\Controllers\ApiAddressController@replace')->where('id', '[0-9]+');
            Route::delete('{id}', 'Totem\SamAddress\App\Controllers\ApiAddressController@destroy')->where('id', '[0-9]+');
        });

    });

});
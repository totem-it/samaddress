<?php

namespace Totem\SamAddress\App\Controllers;

use Illuminate\Auth\AuthManager;
use Totem\SamAddress\App\Requests\AddressReplaceRequest;
use Totem\SamAddress\App\Requests\CustomerBillingAddressRequest;
use Totem\SamAddress\App\Requests\CustomerShippingAddressRequest;
use Totem\SamCustomers\App\Model\Customer;
use Totem\SamAddress\App\Requests\AddressRequest;
use Totem\SamAddress\App\Resources\AddressResource;
use Totem\SamAddress\App\Resources\AddressCollection;
use Totem\SamAddress\App\Resources\BillingAddressCollection;
use Totem\SamAddress\App\Resources\ShippingAddressCollection;
use Totem\SamAddress\App\Repositories\Contracts\AddressRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class ApiAddressController extends ApiController
{

    public function __construct(AddressRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(AddressRequest $request) : AddressResource
    {
        return new AddressResource($this->repository->store($request));
    }

    public function createBilling(CustomerBillingAddressRequest $request) : AddressResource
    {
        return new AddressResource($this->repository->store($request));
    }

    public function createShipping(CustomerShippingAddressRequest $request) : AddressResource
    {
        return new AddressResource($this->repository->store($request));
    }

    public function index(AuthManager $auth) : AddressCollection
    {
        return new AddressCollection(
            $this->getFromRequestQuery(
            ($auth->guard()->user() instanceof Customer) ?
                $this->repository->allByCustomer($auth->guard()->id())
                    :
                $this->repository->all()
            )
        );
    }

    public function indexBilling(AuthManager $auth) : BillingAddressCollection
    {
        return new BillingAddressCollection(
            $this->getFromRequestQuery(
            ($auth->guard()->user() instanceof Customer) ?
                $this->repository->allBillingByCustomer($auth->guard()->id())
                    :
                $this->repository->allBilling()
            )
        );
    }

    public function indexShipping(AuthManager $auth) : ShippingAddressCollection
    {
        return new ShippingAddressCollection(
            $this->getFromRequestQuery(
                ($auth->guard()->user() instanceof Customer) ?
                $this->repository->allShippingByCustomer($auth->guard()->id())
                    :
                $this->repository->allShipping()
            )
        );
    }

    public function show(AuthManager $auth, int $id) : AddressResource
    {
        return new AddressResource(
            $this->getFromRequestQuery(
                ($auth->guard()->user() instanceof Customer) ?
                $this->repository->findByCustomer($auth->guard()->id(), $id)
                    :
                $this->repository->findWithRelationsById($id, ['customer'])
            )
        );
    }

    public function replace(int $id, AddressReplaceRequest $request) : AddressResource
    {
        return new AddressResource($this->repository->store($request, $id));
    }

    public function destroy(AuthManager $auth, int $id) : AddressResource
    {
        return new AddressResource(
        ($auth->guard()->user() instanceof Customer) ?
            $this->repository->deleteByCustomer($auth->guard()->id(), $id)
            :
            $this->repository->delete($id)
        );
    }

}
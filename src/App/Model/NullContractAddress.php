<?php

namespace Totem\SamAddress\App\Model;

class NullContractAddress extends Address
{
    protected $attributes = [
        'country_code' => 'pl'
    ];
}

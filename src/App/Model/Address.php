<?php

namespace Totem\SamAddress\App\Model;

use Illuminate\Database\Eloquent\Model;
use Totem\SamAddress\App\Traits\HasCustomer;
use Totem\SamCore\App\Traits\EloquentDecoratorTrait;

/**
 * @property int id
 * @property string addressable_type
 * @property int addressable_id
 * @property string label
 * @property string firstname
 * @property string lastname
 * @property string company
 * @property string street
 * @property string street_number
 * @property string place_number
 * @property string post_code
 * @property string city
 * @property string country_code
 * @property string phone_number
 * @property string email
 * @mixin \Eloquent
 */
class Address extends Model
{
    use HasCustomer,
        EloquentDecoratorTrait;

    public const model_id = 'addressable_id';
    public const model_type = 'addressable_type';

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'addressable_type',
            'addressable_id',
            'created_at',
            'updated_at',
        ]);

        $this->fillable([
            'addressable_type',
            'addressable_id',
            'label',
            'firstname',
            'lastname',
            'company',
            'street',
            'street_number',
            'place_number',
            'post_code',
            'city',
            'country_code',
            'phone_number',
            'email',
        ]);

        parent::__construct($attributes);

        $this->setTable(config('sam-address.table'));
    }

    public function getFullnameAttribute() : string
    {
        return "{$this->firstname} {$this->lastname}";
    }

    public function getAddressAttribute() : string
    {
        return "{$this->street} {$this->street_number} {$this->place_number}";
    }

    public function newFromBuilder($attributes = [], $connection = null)
    {
        return $this->decorator(self::model_type, [], $attributes, $connection);
    }

}
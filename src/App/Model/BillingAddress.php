<?php

namespace Totem\SamAddress\App\Model;

use Totem\SamAddress\App\Scopes\AddressTypeScope;

class BillingAddress extends Address
{

    protected static function boot() : void
    {
        static::addGlobalScope(new AddressTypeScope(__CLASS__));

        parent::boot();
    }

}
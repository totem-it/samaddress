<?php

namespace Totem\SamAddress\App\Enums;

use BenSampo\Enum\Enum;

final class AddressType extends Enum
{
    public const Billing = \Totem\SamAddress\App\Model\BillingAddress::class;
    public const Shipping = \Totem\SamAddress\App\Model\ShippingAddress::class;
}

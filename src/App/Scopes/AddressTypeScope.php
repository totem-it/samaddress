<?php

namespace Totem\SamAddress\App\Scopes;

use Totem\SamAddress\App\Model\Address;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Scope;

class AddressTypeScope implements Scope
{

    private $type;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function apply(Builder $builder, Model $model) : void
    {
        $builder->where(Address::model_type, '=', $this->type);
    }
}
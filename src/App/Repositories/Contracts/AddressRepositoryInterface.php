<?php

namespace Totem\SamAddress\App\Repositories\Contracts;

use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface AddressRepositoryInterface extends RepositoryInterface
{

    public function allShipping(array $columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc') : \Illuminate\Database\Eloquent\Collection;

    public function allBilling(array $columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc') : \Illuminate\Database\Eloquent\Collection;

    public function allByCustomer(int $customerId): \Illuminate\Database\Eloquent\Collection;

    public function allShippingByCustomer(int $customerId) : \Illuminate\Database\Eloquent\Collection;

    public function allBillingByCustomer(int $customerId) : \Illuminate\Database\Eloquent\Collection;

    public function findByCustomer(int $customerId, int $id = 0, array $columns = ['*']) : \Totem\SamAddress\App\Model\Address;

    public function findWithRelationByCustomer(int $customerId, int $id = 0, array $relationships = [], array $columns = ['*']) : \Totem\SamAddress\App\Model\Address;

    public function store(\Illuminate\Http\Request $request, int $id = 0) : \Totem\SamAddress\App\Model\Address;

    public function deleteByCustomer(int $customerId, int $id = 0) : \Totem\SamAddress\App\Model\Address;

}
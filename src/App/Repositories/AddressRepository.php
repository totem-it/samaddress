<?php

namespace Totem\SamAddress\App\Repositories;

use Totem\SamAddress\App\Enums\AddressType;
use Totem\SamAddress\App\Model\Address;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamAddress\App\Repositories\Contracts\AddressRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|\Totem\SamAddress\App\Model\Address model
 */
class AddressRepository extends BaseRepository implements AddressRepositoryInterface
{

    public function model(): string
    {
        return Address::class;
    }

    public function allShipping(array $columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc') : \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->where($this->model::model_type, AddressType::Shipping)->orderBy($orderBy, $sortBy)->get($columns);
    }

    public function allBilling(array $columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc') : \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->where($this->model::model_type, AddressType::Billing)->orderBy($orderBy, $sortBy)->get($columns);
    }

    public function allByCustomer(int $customerId): \Illuminate\Database\Eloquent\Collection
    {
        if ($customerId === 0) {
            throw new RepositoryException( __('No customer id have been given.') );
        }

        return $this->model->where($this->model::model_id, $customerId)->get();
    }

    public function allShippingByCustomer(int $customerId) : \Illuminate\Database\Eloquent\Collection
    {
        return $this->allByCustomer($customerId)->where($this->model::model_type, AddressType::Shipping);
    }

    public function allBillingByCustomer(int $customerId) : \Illuminate\Database\Eloquent\Collection
    {
        return $this->allByCustomer($customerId)->where($this->model::model_type, AddressType::Billing);
    }

    public function store(\Illuminate\Http\Request $request, int $id = 0) : Address
    {
        $model = ($id === 0) ? $this->model : $this->find($id);

        $model->addressable_type = $request->input('addressable_type');
        $model->addressable_id   = $request->input('addressable_id');
        $model->label            = $request->input('label');
        $model->firstname        = $request->input('firstname');
        $model->lastname         = $request->input('lastname');
        $model->company          = $request->input('company', null);
        $model->street           = $request->input('street');
        $model->street_number    = $request->input('street_number');
        $model->place_number     = $request->input('place_number', null);
        $model->post_code        = $request->input('post_code');
        $model->city             = $request->input('city');
        $model->country_code     = $request->input('country_code');
        $model->phone_number     = $request->input('phone_number');
        $model->email            = $request->input('email');

        $model->save();

        return $model;
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Address
    {
        if ($id === 0) {
            throw new RepositoryException( __('No address id have been given.') );
        }

        $data = $this->model->with($relationships)->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException(  __('Given address id :code is invalid or element not exist.', ['code' => $id]), 404);
        }

        return $data;
    }

    public function findWithRelationByCustomer(int $customerId, int $id = 0, array $relationships = [], array $columns = ['*']) : Address
    {
        $data = $this->findWithRelationsById($id, $relationships, $columns);

        if ($data->addressable_id !== $customerId) {
            throw new RepositoryException(  __('Given address id :code is not correct.', ['code' => $id]), 404);
        }

        return $data;
    }

    public function findByCustomer(int $customerId, int $id = 0, array $columns = ['*']) : Address
    {
        return $this->findWithRelationByCustomer($customerId, $id, [], $columns);
    }

    public function deleteByCustomer(int $customerId, int $id = 0) : Address
    {
        $model = $this->findWithRelationByCustomer($customerId, $id);

        try {
            $model->delete();
            return $model;
        } catch (\Exception $exception) {
            throw new RepositoryException($exception->getMessage());
        }
    }

}
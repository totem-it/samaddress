<?php

namespace Totem\SamAddress\App\Requests;

class AddressReplaceRequest extends AddressRequest
{

    public function rules() : array
    {
        return [
            'label'             => 'required',
            'firstname'         => 'required',
            'lastname'          => 'required',
            'street'            => 'required',
            'street_number'     => 'required',
            'post_code'         => 'required',
            'city'              => 'required',
            'country_code'      => 'required',
            'phone_number'      => 'required',
            'email'             => 'required|email',
        ];
    }

}

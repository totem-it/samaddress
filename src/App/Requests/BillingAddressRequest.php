<?php

namespace Totem\SamAddress\App\Requests;

use Totem\SamAddress\App\Enums\AddressType;

class BillingAddressRequest extends AddressRequest
{

    protected function prepareForValidation() : void
    {
        $this->merge([
            'addressable_type' => AddressType::Billing,
        ]);
    }

}

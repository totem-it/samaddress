<?php

namespace Totem\SamAddress\App\Requests;

class CustomerShippingAddressRequest extends ShippingAddressRequest
{

    protected function prepareForValidation() : void
    {
        $this->merge([
            'label' => "{$this->get('firstname')} {$this->get('lastname')}",
            'addressable_id' => $this->user()->id,
        ]);

        parent::prepareForValidation();
    }

}

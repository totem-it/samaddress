<?php

namespace Totem\SamAddress\App\Requests;

use BenSampo\Enum\Rules\EnumValue;
use Totem\SamAddress\App\Enums\AddressType;
use Totem\SamAdmin\App\Requests\BaseRequest;

class AddressRequest extends BaseRequest
{

    public function rules() : array
    {
        return [
            'label'             => 'required',
            'addressable_id'    => 'required',
            'addressable_type'  => [ 'required', new EnumValue(AddressType::class) ],
            'firstname'         => 'required',
            'lastname'          => 'required',
            'street'            => 'required',
            'street_number'     => 'required',
            'post_code'         => 'required',
            'city'              => 'required',
            'country_code'      => 'required',
            'phone_number'      => 'required',
            'email'             => 'required|email',
        ];
    }

    public function attributes() : array
    {
        return [
            'label'             => __('Label'),
            'firstname'         => __('First name'),
            'lastname'          => __('Last name'),
            'street'            => __('Street'),
            'street_number'     => __('Street number'),
            'post_code'         => __('Post code'),
            'city'              => __('City'),
            'country_code'      => __('Country'),
            'phone_number'      => __('Phone number'),
            'email'             => __('Email'),
            'addressable_type'  => __('Type'),
        ];
    }

}

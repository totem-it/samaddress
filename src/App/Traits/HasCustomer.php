<?php

namespace Totem\SamAddress\App\Traits;

use Totem\SamCustomers\App\Model\Customer;

/**
 * @property null|\Illuminate\Database\Eloquent\Collection|Customer[] customer
 */
trait HasCustomer
{

    public function customer() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Customer::class, self::model_id);
    }

}
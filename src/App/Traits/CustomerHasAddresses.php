<?php

namespace Totem\SamAddress\App\Traits;

use Illuminate\Database\Eloquent\Model;
use Totem\SamAddress\App\Model\Address;
use Totem\SamAddress\App\Model\BillingAddress;
use Totem\SamAddress\App\Model\ShippingAddress;

/**
 * @property \Illuminate\Database\Eloquent\Collection   addresses
 * @property \Illuminate\Database\Eloquent\Collection   shippingAddress
 * @property \Totem\SamAddress\App\Model\BillingAddress billingAddress
 * @property bool has_billing_address
 * @property bool has_shipping_address
 */
trait CustomerHasAddresses
{

    public function addresses() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Address::class, Address::model_id);
    }

    public function shippingAddress() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ShippingAddress::class, Address::model_id);
    }

    public function billingAddress() : \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(BillingAddress::class, Address::model_id);
    }

    public function getHasShippingAddressAttribute() : bool
    {
        return $this->shippingAddress->isNotEmpty();
    }

    public function getHasBillingAddressAttribute() : bool
    {
        return $this->billingAddress !== null;
    }

    public function hasAddress($address) : bool
    {
        return $this->addresses->contains(function ($value) use ($address) {
            return $address === $value->id || str_is($address, $value->label);
        });
    }

    public function attachAddress($address) : void
    {
        $this->hasAddress($address) ?: $this->addresses()->save($address);
    }

    public function attachAddresses($addresses) : void
    {
        foreach ($addresses as $address) {
            $this->attachAddress($address);
        }
    }

    public function detachAddress($address) : void
    {
        if ($this->hasAddress($address)) {
            $this->addresses->find($address)->forceDelete();
        }
    }

    public function detachAddresses($addresses) : void
    {
        foreach ($addresses as $address) {
            $this->detachAddress($address);
        }
    }

}
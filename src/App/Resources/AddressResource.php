<?php

namespace Totem\SamAddress\App\Resources;

use Totem\SamAddress\App\Enums\AddressType;
use Totem\SamCustomers\App\Resources\CustomerResource;
use Totem\SamCore\App\Resources\ApiResource;

class AddressResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'type' => AddressType::getKey($this->{$this->resource::model_type}),
            'label' => $this->label,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'company' => $this->company,
            'street' => $this->street,
            'street_number' => $this->street_number,
            'place_number' => $this->place_number,
            'post_code' => $this->post_code,
            'city' => $this->city,
            'country_code' => $this->country_code,
            'phone_number' => $this->phone_number,
            'email' => $this->email,
            'customer' => $this->when(
                $request->user()->can('customers.show'),
                new CustomerResource($this->whenLoaded('customer'))
            ),
        ];
    }

}

<?php

namespace Totem\SamAddress\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class BillingAddressCollection extends ApiCollection
{

    public $collects = \Totem\SamAddress\App\Resources\AddressResource::class;

}

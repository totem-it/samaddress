<?php

namespace Totem\SamAddress\App\Resources;

use Totem\SamAddress\App\Enums\AddressType;
use Totem\SamCore\App\Resources\ApiCollection;

class AddressCollection extends ApiCollection
{

    public $collects = AddressResource::class;

    public function toArray($request) : array
    {
        return [
            'data' => $this->collection->groupBy(function(AddressResource $address){
                return AddressType::getKey($address->resource->addressable_type);
            })
        ];
    }
}

<?php

namespace Totem\SamAddress;

use Illuminate\Support\ServiceProvider;
use Totem\SamCore\App\Traits\TraitServiceProvider;

class SamAddressServiceProvider extends ServiceProvider
{
    use TraitServiceProvider;

    public function getNamespace() : string
    {
        return 'sam-address';
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() : void
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        $this->publishes([
            __DIR__ . '/config/config.php' => config_path($this->getNamespace().'.php'),
        ], $this->getNamespace().'-config');
        $this->publishes([
            __DIR__ . '/database/migrations' => database_path('migrations'),
        ], $this->getNamespace().'-migrations');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() : void
    {
        $this->mergeConfigFrom(__DIR__ . '/config/config.php', $this->getNamespace());
        $this->registerEloquentFactoriesFrom(__DIR__. '/database/factories');

        if ($this->isWebProject()) {
            $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        }
        $this->loadRoutesFrom(__DIR__.'/routes/api.php');

        $this->configureBinding([
            \Totem\SamAddress\App\Repositories\Contracts\AddressRepositoryInterface::class => \Totem\SamAddress\App\Repositories\AddressRepository::class
        ]);
    }

}

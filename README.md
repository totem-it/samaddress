Sam Address Component
================

This package manage addresses to users or customers in SAM applications.

![Totem.com.pl](https://www.totem.com.pl/files/totem.png)

General System Requirements
-------------
- [PHP >7.1.0](http://php.net/)
- [Laravel ~5.6.*](https://github.com/laravel/framework)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)  
- [SAM-customers ~1.*](https://bitbucket.org/totem-it/samcustomers)  


Quick Installation
-------------
If needed use composer to grab the library

```
$ composer require totem-it/sam-address
```

Usage
-------------

###API

Endpoints use standard filtering from `SAM-core`.

####Customer Endpoints

Get all addresses of authenticated customer. 
```
GET /profile/address-book
```

Get billing address of authenticated customer. 
```
GET /profile/address-book/billing
```

Get shipping addresses of authenticated customer. 
```
GET /profile/address-book/shipping
```

Get address of authenticated customer.
```
GET /profile/address-book/{id}
```

Create new billing address. 
```
POST /profile/address-book/billing
{
    "label" : string,
    "firstname" : string,
    "lastname" : string,
    "street" : string,
    "street_number" : string,
    "post_code" : string,
    "city" : string,
    "country_code" : string,
    "phone_number" : string,
    "email" : string|email,
}
```

Create new shipping address. 
```
POST /profile/address-book/shipping
{
    "label" : string,
    "firstname" : string,
    "lastname" : string,
    "street" : string,
    "street_number" : string,
    "post_code" : string,
    "city" : string,
    "country_code" : string,
    "phone_number" : string,
    "email" : string|email,
}
```

Replace address data. 
```
PUT /profile/address-book/{id}
{
    "label" : string,
    "firstname" : string,
    "lastname" : string,
    "street" : string,
    "street_number" : string,
    "post_code" : string,
    "city" : string,
    "country_code" : string,
    "phone_number" : string,
    "email" : string|email,
}
```

Delete address. 
```
DELETE /profile/address-book/{id}
```

Authors
-------------

* **Borys Zmuda** - Lead designer - [GoldenLine](http://www.goldenline.pl/borys-zmuda/)
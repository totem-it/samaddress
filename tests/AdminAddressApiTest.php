<?php

namespace Totem\SamAddress\Tests;

use Totem\SamAddress\App\Model\Address;
use Totem\SamAddress\App\Enums\AddressType;
use Totem\SamAdmin\Tests\ApiCrudTest;

class AdminAddressApiTest extends ApiCrudTest
{

    protected $model = Address::class;
    protected $endpoint = 'customers/addresses';
    protected $additionalFields = [
        'addressable_id' => 1,
        'addressable_type' => AddressType::Billing
    ];
    protected $withoutFields = [

    ];

    protected function createModel(array $attributes = [])
    {
        $this->loginAs($this->setUser());

        return factory($this->model)->create([
            'addressable_id' => $this->loginUser->id
        ]);
    }

    public function testBillingIndex() : void
    {
        $response = $this->json('GET', "/api/{$this->endpoint}/billing");

        $response
            ->assertJson([
                'data' => []
            ])
            ->assertOk()
        ;
    }

    public function testShippingIndex() : void
    {
        $response = $this->json('GET', "/api/{$this->endpoint}/shipping");

        $response
            ->assertJson([
                'data' => []
            ])
            ->assertOk()
        ;
    }
}

<?php

namespace Totem\SamAddress\Tests;

use Totem\SamAddress\App\Model\Address;
use Totem\SamAddress\App\Enums\AddressType;
use Totem\SamCustomers\Tests\AttachCustomerJwtToken;
use Totem\SamAdmin\Tests\ApiCrudTest;

class AddressApiTest extends ApiCrudTest
{
    use AttachCustomerJwtToken;

    protected $model = Address::class;
    protected $endpoint = 'profile/address-book';
    protected $additionalFields = [
        'addressable_id' => 1,
        'addressable_type' => AddressType::Billing
    ];
    protected $withoutFields = [
        'firstname'
    ];

    protected function createModel(array $attributes = [])
    {
        $this->loginAs($this->setUser());

        return factory($this->model)->create([
            'addressable_id' => $this->loginUser->id
        ]);
    }

    public function testBillingIndex() : void
    {
        $response = $this->json('GET', "/api/{$this->endpoint}/billing");

        $response
            ->assertJson([
                'data' => []
            ])
            ->assertOk()
        ;
    }

    public function testShippingIndex() : void
    {
        $response = $this->json('GET', "/api/{$this->endpoint}/shipping");

        $response
            ->assertJson([
                'data' => []
            ])
            ->assertOk()
        ;
    }

    public function testEndpointPostStore() : void
    {
        $this->call('POST', "/api/{$this->endpoint}/billing", $this->modelForStore())->assertDontSee('"code":404')->assertDontSee('"code":405');
        $this->call('POST', "/api/{$this->endpoint}/shipping", $this->modelForStore())->assertDontSee('"code":404')->assertDontSee('"code":405');
    }

    public function testStore() : void
    {
        $this->json('POST', "/api/{$this->endpoint}/billing", $this->modelForStore())
            ->assertJson([
                'data' => []
            ])
            ->assertStatus(201);
        $this->json('POST', "/api/{$this->endpoint}/shipping", $this->modelForStore())
            ->assertJson([
                'data' => []
            ])
            ->assertStatus(201);
    }

    public function testFailedValidationStore() : void
    {
        $this->json('POST', "/api/{$this->endpoint}/billing", $this->arrayModel(true))
            ->assertJson([
                'error' => [],
            ])
            ->assertStatus(422);

        $this->json('POST', "/api/{$this->endpoint}/shipping", $this->arrayModel(true))
            ->assertJson([
                'error' => [],
            ])
            ->assertStatus(422);
    }
}
